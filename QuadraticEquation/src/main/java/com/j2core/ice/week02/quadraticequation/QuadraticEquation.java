package com.j2core.ice.week02.quadraticequation;

import java.util.Scanner;
/**
 * Solves Quadratic equation.
 *
 * @author Serge Kotvitsky on 5/26/16.
 * @version 1.0
 */
public class QuadraticEquation {

    private final static double ADMISSIBLE_ERROR = 0.00001;
    /**
     * Input a b c
     *
     * @return value of input variable
     */
    private double getArgument () {

        String str;
        Double value;
        Scanner scan = new Scanner(System.in);

        while (true) {
            str = scan.next();
            try {
                value = Double.parseDouble(str);
                if (value.equals(Double.NaN)
                        || value.equals(Double.NEGATIVE_INFINITY)
                        || value.equals(Double.POSITIVE_INFINITY)) {
                    throw new NumberFormatException();
                }

                return value;

            } catch (Exception e) {

                System.out.println("The entered value is not a number.Re-enter the data");
            }
        }
    }


    /**
     * Calculate the roots of a quadratic equation.
     *
     * @param a,b,c coefficients of the quadratic equation
     */

    private void calc(double a, double b, double c) {

        double x1, x2, discr = b * b - 4 * a * c;

        if (a == 0) {
            if (b != 0) {
                x1 = -c / b;
                System.out.println("Linear equation " + "x1= " + x1);
            } else {
                System.out.print("It is not equation");
            }

        } else if (Math.abs(discr) < ADMISSIBLE_ERROR) {

            x1 = -b / 2 / a;
            System.out.println("x1 = x2 = " + x1);

        } else if (discr > 0) {

            double temp = Math.sqrt(discr);
            x1 = (-b + temp) / (2 * a);
            x2 = (-b - temp) / (2 * a);
            System.out.println("x1 = " + x1 + "  " + "x2 = " + x2);

        } else {

            x1 = -b / 2 / a;
            x2 = Math.sqrt(-discr) / (2 * a);
            System.out.println("Equation has complex roots");
            System.out.println("X1 = " + x1 + " + " + x2 + "i");
            System.out.println("X2 = " + x1 + " - " + x2 + "i");

        }
    }


    public static void main(String[] args) {

        double a, b, c;

        QuadraticEquation equation = new QuadraticEquation();

        System.out.print("a = ");
        a = equation.getArgument();

        System.out.print("b = ");
        b = equation.getArgument();

        System.out.print("c = ");
        c = equation.getArgument();

        equation.calc(a, b, c);

    }

}
